# Citrix Storefront Custom Connector (with optional TOTP)

The Citrix Storefront Custom Connector is a Login Enterprise custom connector build in Powershell and Selenium.

# Features

## Support for most any browser (Selenium and Webdriver based)

This Citrix Storefront Custom Connector was built to use webdrivers (vs activex/com). This should allow for better currency (add any version of webdriver/browser you wish) and cross browser support (edge, chrome, internet explorer, firefox, what have you).

## TOPT Support

Through the use of some other projects (submodules), this custom connector can be used with MFA schemes that support time-based one time passwords (TOTP). The current project was modeled with TOTP based on OKTA and shows how you can drive those capabilities. No support for push based MFA has been added at this time.

# Roadmap

## Page Objects

It is in the "plan" to factor a page objects based example. You can write your code however you wish, this connector really serves as an implementation example, and we will include additional examples in the future that illustrate approaches.

## Secrets/Vault support

As with any sensitive information, where and how you store it is important. Powershell has a secrets module that we are chomping at the bit to get started with and integrate.

## Push MFA support

To support a wider array of options we will look into a server component that can interact with other MFA options, like text or other push notifications. RnD has not started on this, so take it with a grain of salt.

# Dependencies

If you clone the repo, clone with subs. This project will leverage the awesome Selenium-Powershell project by Adam Driscoll, and by virtue inherets the dependencies therein. This is not a requirement of course, but this project may make some assumptions based on Selenium-Powershell and Selenium-Powershell is a submodule.

# Installation

To install, clone this repo (optionally with --recurse-submodules) on your launcher and use the configuration file to point to the Selenium-Powershell instance.

# Configure Login Enterprise

TODO

# License

This project uses the permissive MIT License

# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change. 

Please note we have a code of conduct, please follow it in all your interactions with the project.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a 
   build.
2. Update the README.md with details of changes to the interface, this includes new environment 
   variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this
   Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).
4. You may merge the Pull Request in once you have the sign-off of two other developers, or if you 
   do not have permission to do that, you may request the second reviewer to merge it for you.

## Code of Conduct

### Our Pledge

In the interest of fostering an open and welcoming environment, we as
contributors and maintainers pledge to making participation in our project and
our community a harassment-free experience for everyone, regardless of age, body
size, disability, ethnicity, gender identity and expression, level of experience,
nationality, personal appearance, race, religion, or sexual identity and
orientation.

### Our Standards

Examples of behavior that contributes to creating a positive environment
include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or
advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting

### Our Responsibilities

Project maintainers are responsible for clarifying the standards of acceptable
behavior and are expected to take appropriate and fair corrective action in
response to any instances of unacceptable behavior.

Project maintainers have the right and responsibility to remove, edit, or
reject comments, commits, code, wiki edits, issues, and other contributions
that are not aligned to this Code of Conduct, or to ban temporarily or
permanently any contributor for other behaviors that they deem inappropriate,
threatening, offensive, or harmful.

### Scope

This Code of Conduct applies both within project spaces and in public spaces
when an individual is representing the project or its community. Examples of
representing a project or community include using an official project e-mail
address, posting via an official social media account, or acting as an appointed
representative at an online or offline event. Representation of a project may be
further defined and clarified by project maintainers.

### Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported by contacting the project team at [INSERT EMAIL ADDRESS]. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. The project team is
obligated to maintain confidentiality with regard to the reporter of an incident.
Further details of specific enforcement policies may be posted separately.

Project maintainers who do not follow or enforce the Code of Conduct in good
faith may face temporary or permanent repercussions as determined by other
members of the project's leadership.

### Attribution

This Code of Conduct is adapted from the [Contributor Covenant][homepage], version 1.4,
available at [http://contributor-covenant.org/version/1/4][version]

[homepage]: http://contributor-covenant.org
[version]: http://contributor-covenant.org/version/1/4/