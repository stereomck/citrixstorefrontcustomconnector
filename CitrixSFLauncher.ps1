<#
<<<<<<< HEAD
.SYNOPSIS
    Launch HDX session to a published resource through StoreFront or NetScaler Gateway (integrated with StoreFront).
.DESCRIPTION

    This script launches an HDX session to a published resource through StoreFront or NetScaler Gateway (integrated with StoreFront).

    It attempts to closely resemble what an actual user would do by:
    -Opening a browser.
    -Navigating directly to the Receiver for Web site or NetScaler Gateway portal.
    -Completing the fields.
    -Logging in.
    -Clicking on the resource.
    -Logging off the StoreFront site.

    Requirements:
    -Use an Administrator console of PowerShell.
    -SiteURL should be part of the Intranet Zone (or Internet Zone at Medium-Low security) in order to be able to download AND launch the ICA file. This can be done through a GPO.
    -StoreFront 2.0 or higher.
    -If using NetScaler Gateway, version 9.3 or higher.
    -Changes in web.config under C:\inetpub\wwwroot\Citrix\<storename>Web\: autoLaunchDesktop to false, pluginAssistant to false and logoffAction to none.
    -Currently works for desktops or already subscribed apps only. You can auto subscribe users to apps by setting "KEYWORDS:Auto" in the published app's description.

    By default, the script creates a log file with the username like SFLauncher_username.log.
.PARAMETER SiteURL
    The complete URL of the StoreFront Receiver for Web site or NetScaler Gateway portal.
.PARAMETER UserName
    The name of the user which is used to log on. Acceptable forms are down-level logon name or user principal name.
.PARAMETER Password
    The password of the user which is used to log on.
.PARAMETER ResourceName
    The display name of the resource to be launched.
.PARAMETER SleepBeforeLogoff
    The time in seconds to sleep after clicking the resource and before logging off. Default is 5.
.PARAMETER NumberOfRetries
    The number of retries when retrieving an element. Default is 30.
.PARAMETER LogFilePath
    Directory path to where the log file will be saved. Default is SystemDrive\Temp.
.PARAMETER LogFileName
    File name for the log file. Default is SFLauncher_<UserName>.log.
.PARAMETER NoLogFile
    Specify to disable logging to a file.
.PARAMETER NoConsoleOutput
    Specify to disable logging to the console.
.PARAMETER TwoFactorAuth
    The token or password used for two-factor authentication. This is used in the NetScaler Gateway portal or third party authentication (ie OKTA)
.EXAMPLE
    CitrixSFLauncher.ps1 -SiteURL "http://storefront.domain.com" -UserName "domain1\User1" -Password "P4ssw0rd" -ResourceName "My Desktop"

    Description
    -----------
    Launches a session to a resource using the parameters provided.
.LINK
    UserName format used in StoreFront.
    http://msdn.microsoft.com/en-us/library/windows/desktop/aa380525(v=vs.85).aspx#down_level_logon_name
.LINK
    Change to autoLaunchDesktop.
    http://support.citrix.com/proddocs/topic/dws-storefront-20/dws-configure-wr-view.html
.LINK
    Change to logoffAction.
    http://support.citrix.com/proddocs/topic/dws-storefront-20/dws-configure-wr-workspace.html
.NOTES
    Copyright (c) Citrix Systems, Inc. All rights reserved.
        Version 1.3
=======
  .SYNOPSIS
      Launch HDX session to a published resource through StoreFront or NetScaler Gateway (integrated with StoreFront).
  .DESCRIPTION

      This script launches an HDX session to a published resource through StoreFront or NetScaler Gateway (integrated with StoreFront).

      It attempts to closely resemble what an actual user would do by:
      -Opening a browser.
      -Navigating directly to the Receiver for Web site or NetScaler Gateway portal.
      -Completing the fields.
      -Logging in.
      -Clicking on the resource.
      -Logging off the StoreFront site.

      Requirements:
      -Use an Administrator console of PowerShell.
      -SiteURL should be part of the Intranet Zone (or Internet Zone at Medium-Low security) in order to be able to download AND launch the ICA file. This can be done through a GPO.
      -StoreFront 2.0 or higher.
      -If using NetScaler Gateway, version 9.3 or higher.
      -Changes in web.config under C:\inetpub\wwwroot\Citrix\<storename>Web\: autoLaunchDesktop to false, pluginAssistant to false and logoffAction to none.
      -Currently works for desktops or already subscribed apps only. You can auto subscribe users to apps by setting "KEYWORDS:Auto" in the published app's description.

      By default, the script creates a log file with the username like SFLauncher_username.log.
  .PARAMETER SiteURL
      The complete URL of the StoreFront Receiver for Web site or NetScaler Gateway portal.
  .PARAMETER UserName
      The name of the user which is used to log on. Acceptable forms are down-level logon name or user principal name.
  .PARAMETER Password
      The password of the user which is used to log on.
  .PARAMETER ResourceName
      The display name of the resource to be launched.
  .PARAMETER SleepBeforeLogoff
      The time in seconds to sleep after clicking the resource and before logging off. Default is 5.
  .PARAMETER NumberOfRetries
      The number of retries when retrieving an element. Default is 30.
  .PARAMETER LogFilePath
      Directory path to where the log file will be saved. Default is SystemDrive\Temp.
  .PARAMETER LogFileName
      File name for the log file. Default is SFLauncher_<UserName>.log.
  .PARAMETER NoLogFile
      Specify to disable logging to a file.
  .PARAMETER NoConsoleOutput
      Specify to disable logging to the console.
  .PARAMETER TwoFactorAuth
      The token or password used for two-factor authentication. This is used in the NetScaler Gateway portal or third party authentication (ie OKTA)
  .EXAMPLE
      CitrixSFLauncher.ps1 -SiteURL "http://storefront.domain.com" -UserName "domain1\User1" -Password "P4ssw0rd" -ResourceName "My Desktop"

      Description
      -----------
      Launches a session to a resource using the parameters provided.
  .LINK
      UserName format used in StoreFront.
      http://msdn.microsoft.com/en-us/library/windows/desktop/aa380525(v=vs.85).aspx#down_level_logon_name
  .LINK
      Change to autoLaunchDesktop.
      http://support.citrix.com/proddocs/topic/dws-storefront-20/dws-configure-wr-view.html
  .LINK
      Change to logoffAction.
      http://support.citrix.com/proddocs/topic/dws-storefront-20/dws-configure-wr-workspace.html
  .NOTES
      Copyright (c) Citrix Systems, Inc. All rights reserved.
      Version 1.3
>>>>>>> 020d7f68926e90c639b5f61b5ec7f7169c46c4b6
#>

Param (
     [Parameter(Mandatory=$true,Position=0)] [string]$SiteURL,
     [Parameter(Mandatory=$true,Position=1)] [string]$UserName,
     [Parameter(Mandatory=$true,Position=2)] [string]$Password,
     [Parameter(Mandatory=$true,Position=3)] [string]$ResourceName,
     [Parameter(Mandatory=$false)] [int]$SleepBeforeLogoff = 5,
     [Parameter(Mandatory=$false)] [int]$NumberOfRetries = 30,
     [Parameter(Mandatory=$false)] [string]$LogFilePath = "$env:TEMP",
     [Parameter(Mandatory=$false)] [string]$LogFileName = "SFLauncher_$($UserName.Replace('\','_')).log",
     [Parameter(Mandatory=$false)] [switch]$NoLogFile,
     [Parameter(Mandatory=$false)] [switch]$NoConsoleOutput,
     [Parameter(Mandatory=$false)] [string]$TwoFactorAuthKeyFile
 )

# Variables
<<<<<<< HEAD
$ScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
$DownloadDir = $ScriptDirectory
if ( $null -eq $TwoFactorAuthKeyFile ) { $TwoFactorAuthKeyFile = "$ScriptDirectory\TwoFactorKeyFile.csv" }
=======
$DownloadDir = "$pwd"
$ScriptDirectory = Split-Path -Path $MyInvocation.MyCommand.Definition -Parent
>>>>>>> 020d7f68926e90c639b5f61b5ec7f7169c46c4b6

# Element mappings
$OKTA_Login_Username_Textbox_Id = "idp-discovery-username"
$OKTA_Login_Username_Submit = 'idp-discovery-submit'
$OKTA_Login_Password_TextboxId = "okta-signin-password"
$OKTA_Login_Submit_ButtonId = "okta-signin-submit"
$OKTA_Verify_Code_Textbox_Id = "input4"
$OKTA_Verify_Code_Submit_Button_XPath = "//input[@type='submit']"
$ResourceLocator = '//*[@alt=' + $ResourceName + ']'
<<<<<<< HEAD

=======
>>>>>>> 020d7f68926e90c639b5f61b5ec7f7169c46c4b6

function Write-SFLauncherHeader {
    $infoMessage = @"
                           *****************************************************
                           *****  Citrix Solutions Lab SF Launcher Script  *****
                           *****        -Console output:     {0,-6}        *****
                           *****        -Log output:         {1,-6}        *****
                           *****************************************************
"@  -f $(-not $NoConsoleOutput),$(-not $NoLogFile)
    Write-Host $infoMessage
}

function Write-ToSFLauncherLog {
    Param (
        [Parameter(Mandatory=$true,ValueFromPipeline=$true)] [string]$Message,
        [Parameter(Mandatory=$false)] [string]$LogFile=$($LogFilePath.TrimEnd('\') + "\$LogFileName"),
        [Parameter(Mandatory=$false)] [bool]$NoConsoleOutput=$NoConsoleOutput,
        [Parameter(Mandatory=$false)] [bool]$NoLogFile=$NoLogFile
    )
    Begin {
        if(Test-Path $LogFile -IsValid) {
            if(!(Test-Path "$LogFile" -PathType Leaf)) {
                New-Item -Path $LogFile -ItemType "file" -Force -ErrorAction Stop | Out-Null
            }
        } else {
            throw "Log file path is invalid"
        }
    }
    Process {
        $Message = [DateTime]::Now.ToString("[MM/dd/yyy HH:mm:ss.fff]: ") + $Message

        if (-not $NoConsoleOutput) {
            Write-Host $Message
        }

        if (-not $NoLogFile) {
            $Message | Out-File -FilePath $LogFile -Append
        }
    }
}

function Initialize-Script {
    Write-ToSFLauncherLog "Initializing Script"
    Import-Module "$ScriptDirectory\selenium-powershell\Selenium.psd1" -force
    try {
      . ("$ScriptDirectory\selenium-powershell\SeleniumClasses.ps1")
    }
    catch {
        Write-Host "Error while loading supporting PowerShell Scripts"
    }

    # Check for an imported type to confirm load
    if (-not ([System.Management.Automation.PSTypeName]'ValidateURIAttribute').Type)
    {
       Write-Host "No Type"
    }else{
       Write-Host "Type exists"
    }

}

function Initialize-Browser {
    Write-ToSFLauncherLog "Initializing Browser"
    $global:Driver = Start-SeChrome -DefaultDownloadPath "$DownloadDir\$guid"

    Enter-SeUrl $SiteUrl -Driver $Driver
}

function Submit-DetectCitrixReciever {
    Write-ToSFLauncherLog "Clicking Detect"
    $waitDetect = Find-SeElement -Driver $Driver -Wait -Timeout 60 -Id 'protocolhandler-welcome-installButton' -ErrorAction SilentlyContinue
    $Element = Find-SeElement -Driver $Driver -Id 'protocolhandler-welcome-installButton'
    Invoke-SeClick -Element $Element
}

function Submit-OKTALogin {
    Write-ToSFLauncherLog "Submitting Login"
    $waitLogin = Find-SeElement -Driver $Driver -Wait -Timeout 60 -id $OKTA_Login_Username_Textbox_Id
    $Element = Find-SeElement -Driver $Driver -Id $OKTA_Login_Username_Textbox_Id
    Send-SeKeys -Element $Element -Keys $UserName
    $Element = Find-SeElement -Driver $Driver -Wait -Timeout 60 -id $OKTA_Login_Username_Submit
    Send-SeClick -Element $Element
    $Element = Find-SeElement -Driver $Driver -Id $OKTA_Login_Password_TextboxId
    Send-SeKeys -Element $Element -Keys $Password
    $Element = Find-SeElement -Driver $Driver -Id $OKTA_Login_Submit_ButtonId
    Invoke-SeClick -Element $Element
}

function Submit-VerifyCode {
    Write-ToSFLauncherLog "Submitting Verify Code"
    $waitVerify = Find-SeElement -Driver $Driver -Wait -Timeout 60 -id $OKTA_Verify_Code_Textbox_Id -ErrorAction SilentlyContinue
    If($null -ne $waitDetect){
      $TOTPCode = Get-TOTPCode
      $Element = Find-SeElement -Driver $Driver -Id $OKTA_Verify_Code_Textbox_Id
      Send-SeKeys -Element $Element -Keys $TOTPCode
      $Element = Find-SeElement -Driver $Driver -XPath $OKTA_Verify_Code_Submit_Button_XPath
      Invoke-SeClick -Element $Element
   }
   Else{
      Write-Host "Verification Page not found. Skipping..."
   }
}

function Get-TwoFactorAuthKey {
    $TwoFactorAuthKey = ($TwoFactorKeyFile = Import-Csv -Path $TwoFactorAuthKeyFile) | where { $_.username } | Select key; return $TwoFactorKeyFile.key
}

function Get-TOTPCode {
    Write-ToSFLauncherLog "Getting TOTP Code"
    Set-StrictMode -Version Latest
    Write-ToSFLauncherLog "Getting Key from file"
    $TwoFactorAuthKey = Get-TwoFactorAuthKey

    $cmdPath = "$ScriptDirectory\bin\TOTPGenerator.exe"
    $cmdArgList = @(
	    "-k",
        "$TwoFactorAuthKey"
    )
    $totp = & $cmdPath $cmdArgList
    return $totp
}

function Click-ResourceCannotStartOK {
    Write-ToSFLauncherLog "Clicking OK Button on Default Resource Start Error"
    $waitSF = Find-SeElement -Driver $Driver -Wait -Timeout 60 -LinkText "OK" -ErrorAction SilentlyContinue
    $Element = Find-SeElement -Driver $Driver -LinkText "OK"
    Invoke-SeClick -Element $Element
}

function Start-Resource {
    Write-ToSFLauncherLog "Starting Resource"
    #$waitSF = Find-SeElement -Driver $Driver -Wait -Timeout 60 -LinkText $ResourceName
    #$Element = Find-SeElement -Driver $Driver -LinkText $ResourceName
    $waitResource = Find-SeElement -Driver $Driver -XPath $ResourceLocator
    $Element = Find-SeElement -Driver $Driver -XPath $ResourceLocator
    Invoke-SeClick -Element $Element
}

function Confirm-ICA {
    Write-ToSFLauncherLog "Confirming ICA Launch"
    Get-Process -Name "wfica32"
    start-sleep 20
}

function Get-ChromeDownloadFilename {

   Write-Host "Getting Download Filename from Chrome"
   $downloadsPage = "chrome://downloads"

   # open 2nd tab and nav to downloads
   $openTab = $Driver.ExecuteScript("window.open()")
   $switchTab = $Driver.SwitchTo().Window($Driver.WindowHandles[-1])
   $navTab = $Driver.Navigate().GoToUrl("chrome://downloads")

   # get the latest downloaded file name
   $fileName = $Driver.ExecuteScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div#content  #file-link').text")

   # get the latest downloaded file url
   $sourceURL = $Driver.ExecuteScript("return document.querySelector('downloads-manager').shadowRoot.querySelector('#downloadsList downloads-item').shadowRoot.querySelector('div#content  #file-link').href")

   # close the downloads tab2
   $Driver.Close()

   return $fileName
}

function Get-ICAFileName {
   Write-Host "Getting Downloaded ICA Filename"
   $browser = $Driver.Capabilities.BrowserName
   Switch ($browser)
   {
       "chrome" { Get-ChromeDownloadFilename }
   }
}

function Launch-ICAFile {
   param (
       [Parameter(Mandatory=$true)] [string]$file
   )
   Write-Host "Launching ICA File"
   Write-Host $file
   Start-Process -FilePath $file

}

try {
    Write-ToSFLauncherLog "*************** LAUNCHER SCRIPT BEGIN ***************"
    $guid = New-Guid

    Initialize-Script
    Initialize-Browser

    #Submit-DetectCitrixReciever
    Submit-OKTALogin
    Submit-VerifyCode
    Start-Resource
    $icaFileName = Get-ICAFileName
    Launch-ICAFile -file "$DownloadDir\$guid\$icaFileName"

    Confirm-ICA

}
catch {
    Write-ToSFLauncherLog "Exception caught by script"
    $_.ToString() | Write-Host
    $_.InvocationInfo.PositionMessage | Write-Host
    throw $_
}
finally {
    Write-ToSFLauncherLog "All commands complete. Shutting down"
    Stop-SeDriver -Driver $Driver
    Write-ToSFLauncherLog "***************  LAUNCHER SCRIPT END  ***************"
}